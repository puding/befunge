# Befunge-93 Code Editor and Interpreter

[Befunge](https://en.wikipedia.org/wiki/Befunge) is an esoteric programming language. Its
distinguishing feature is that the code consists of single-character commands arranged in a 2D grid.

This project provides a simple editor of Befunge programs. It is able to interpret (run) your code,
too. It's written in Vue: I do realize that Vue may not be the ideal framework for this project and
the code quality is lower than my current standars, but I wrote this as a side-project when I was
very inexperienced.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
